<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;
use League\Fractal\TransformerAbstract;

Craft::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', '*');

class Project extends TransformerAbstract
{
    public function transform(Entry $entry)
    {
        $logo = !empty($entry->logo[0]) ? $entry->logo[0]->url : null;

        $screensaver = [];
        foreach ($entry->screensaver->all() as $block) {
            switch ($block->type->handle) {
                case 'singleImage':
                    $screensaver[] = [
                        'type' => $block->type->handle,
                        'asset' => $block->asset->one()->url,
                        'heading' => $block->heading,
                        'subheading' => $block->subheading,
                    ];
                    break;
                case 'multipleImages':
                    $assets = [];
                    foreach ($block->assets->all() as $asset) {
                        $assets[] = $asset->url;
                    }
                    $screensaver[] = [
                        'type' => $block->type->handle,
                        'assets' => $assets,
                        'heading' => $block->heading,
                        'subheading' => $block->subheading,
                    ];
                    break;
                case 'video':
                    $screensaver[] = [
                        'type' => $block->type->handle,
                        'vimeoId' => $block->vimeoId,
                        'heading' => $block->heading,
                        'subheading' => $block->subheading,
                    ];
                    break;
                case 'color':
                    $screensaver[] = [
                        'type' => $block->type->handle,
                        'color' => $block->color->getHex(),
                        'heading' => $block->heading,
                        'subheading' => $block->subheading,
                    ];
                    break;
            }
        }

        $child = $entry->getChildren();
        $slug = !empty($child[0]) ? $child[0]->slug : null;

        return [
            'entryType' => $entry->type->name,
            'title' => $entry->title,
            'url' => "/{$entry->slug}/{$slug}",
            'logo' => $logo,
            'headerColor' => $entry->headerColor->getHex(),
            'theme' => $entry->darkLightTheme,
            'screensaver' => $screensaver,
            'jsonUrl' => UrlHelper::url("projects/{$slug}.json"),
        ];
    }
}

class MainMenu extends TransformerAbstract
{
    public function transform(Entry $entry)
    {
        $project = $entry->ancestors->one();
        $headerColor = $project ? $project->headerColor->getHex() : null;

        $children = $entry->getChildren();
        $entries = [];
        foreach ($children->all() as $entry) {
            $thumbnailBg = [];
            if ($entry->thumbnailBg) {
              foreach ($entry->thumbnailBg->all() as $block) {
                  switch ($block->type->handle) {
                      case 'image':
                          $thumbnailBg[] = [
                              'type' => $block->type->handle,
                              'asset' => $block->asset->one()->url,
                          ];
                          break;
                      case 'video':
                          $thumbnailBg[] = [
                              'type' => $block->type->handle,
                              'vimeoId' => $block->vimeoId,
                          ];
                          break;
                      case 'color':
                          $thumbnailBg[] = [
                              'type' => $block->type->handle,
                              'color' => $block->color->getHex(),
                          ];
                          break;
                  }
              }
            }

            $ancestorsSlugs = null;
            foreach ($entry->ancestors->all() as $ancestor) {
                $ancestorsSlugs .= "/$ancestor->slug";
            }

            $entries[] = [
                'entryType' => $entry->type->name,
                'title' => $entry->title,
                'thumbnailSubheading' => $entry->thumbnailSubheading,
                'url' => parse_url($entry->url)['path'],
                'thumbnailBg' => $thumbnailBg,
                'jsonUrl' => UrlHelper::url("projects/$entry->slug.json"),
            ];
        }

        return [
            'title' => 'Main Menu',
            'projectTitle' => $project->title,
            'logo' => !empty($project->logo[0]) ? $project->logo[0]->url : null,
            'headerColor' => $headerColor,
            'slides' => $entries,
        ];
    }
}

class Folder extends TransformerAbstract
{
    public function transform(Entry $entry)
    {
        $project = $entry->ancestors->one();
        $headerColor = $project ? $project->headerColor->getHex() : null;

        $parent = $entry->getParent();

        // Breadcrumbs
        $ancestors = [];
        foreach ($entry->ancestors->all() as $key=>$ancestor) {
            if ($key == 1) {
              $title = 'Main Menu';
            } else {
              $title = $ancestor->title;
            }

            $ancestors[] = [
              'title' => $title,
              'url' => parse_url($ancestor->url)['path'],
            ];
        }

        // Current location
        $ancestors[] = [
          'title' => $entry->title,
        ];

        $children = $entry->getChildren();
        $entries = [];
        foreach ($children->all() as $media) {
            $thumbnailBg = [];
            if ($media->thumbnailBg) {
              foreach ($media->thumbnailBg->all() as $block) {
                  switch ($block->type->handle) {
                      case 'image':
                          $thumbnailBg[] = [
                              'asset' => $block->asset->one()->url,
                          ];
                          break;
                      case 'video':
                          $thumbnailBg[] = [
                              'vimeoId' => $block->vimeoId,
                          ];
                          break;
                      case 'color':
                          $thumbnailBg[] = [
                              'color' => $block->color->getHex(),
                          ];
                          break;
                  }
              }
            }

            $entries[] = [
              'title' => $media->title,
              'url' => parse_url($media->url)['path'],
              'entryType' => $media->type->name,
              'thumbnailBg' => $thumbnailBg,
              'jsonUrl' => UrlHelper::url("projects/{$media->slug}.json"),
            ];
        }

        return [
            'entryType' => $entry->type->name,
            'title' => $entry->title,
            'headerColor' => $headerColor,
            'backLink' => parse_url($parent->url)['path'],
            'breadcrumbs' => $ancestors,
            'slides' => $entries,
        ];
    }
}

class Media extends TransformerAbstract
{
    public function transform(Entry $entry)
    {
        $project = $entry->ancestors->one();
        $headerColor = $project ? $project->headerColor->getHex() : null;

        $parent = $entry->getParent();

        $ancestors = [];
        foreach ($entry->ancestors->all() as $key=>$ancestor) {
            if ($key == 1) {
              $title = 'Main Menu';
            } else {
              $title = $ancestor->title;
            }

            $ancestors[] = [
              'title' => $title,
              'url' => parse_url($ancestor->url)['path'],
            ];
        }

        $ancestors[] = [
          'title' => $entry->title,
        ];

        switch ($entry->type) {
            case 'video':
                return [
                    'entryType' => $entry->type->name,
                    'headerColor' => $headerColor,
                    'backLink' => parse_url($parent->url)['path'],
                    'breadcrumbs' => $ancestors,
                    'vimeoId' => $entry->vimeoId,
                ];
                break;
            case 'imageGallery':
                $imageAssets = [];
                foreach ($entry->imageAssets->all() as $asset) {
                    $imageAssets[] = $asset->url;
                }

                return [
                    'entryType' => $entry->type->name,
                    'headerColor' => $headerColor,
                    'backLink' => parse_url($parent->url)['path'],
                    'breadcrumbs' => $ancestors,
                    'imageAssets' => $imageAssets,
                ];
                break;
            case 'pdf':
                $pdf = !empty($entry->pdfAsset[0]) ? $entry->pdfAsset[0]->url : null;

                return [
                    'entryType' => $entry->type->name,
                    'headerColor' => $headerColor,
                    'backLink' => parse_url($parent->url)['path'],
                    'breadcrumbs' => $ancestors,
                    'pdfAsset' => $pdf,
                    'issuuUrl' => $entry->issuuUrl,
                ];
                break;
            case 'website':
                return [
                    'entryType' => $entry->type->name,
                    'headerColor' => $headerColor,
                    'backLink' => parse_url($parent->url)['path'],
                    'breadcrumbs' => $ancestors,
                    'siteUrl' => $entry->siteUrl,
                ];
                break;
        }
    }
}

return [
    'defaults' => [
        'elementType' => Entry::class,
    ],
    'endpoints' => [
        'projects.json' => function() {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'type' => 'project'
                ],
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => parse_url($entry->url)['path'],
                        'slug' => $entry->slug,
                        'jsonUrl' => UrlHelper::url("projects/{$entry->slug}.json"),
                    ];
                },
            ];
        },

        'projects/<slug:{slug}>.json' => function($slug) {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'slug' => $slug,
                ],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $type = $entry->type->name;

                    if (
                      $type == 'Website' ||
                      $type == 'Video' ||
                      $type == "PDF" ||
                      $type == "Image Gallery"
                    ) {
                      $type = 'Media';
                    }

                    $typeClass = preg_replace('/\s/u', '', $type);

                    $object = new $typeClass();
                    return $object->transform($entry, new $typeClass);
                },
            ];
        },
    ]
];
