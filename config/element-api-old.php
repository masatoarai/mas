<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;

Craft::$app->getResponse()->getHeaders()->set('Access-Control-Allow-Origin', '*');

return [
    'defaults' => [
        'elementType' => Entry::class,
    ],
    'endpoints' => [
        'projects.json' => function() {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'type' => 'project'
                ],
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        // 'url' => "/{$entry->slug}",
                        'url' => $entry->url,
                        'slug' => $entry->slug,
                        'jsonUrl' => UrlHelper::url("projects/{$entry->slug}.json"),
                    ];
                },
            ];
        },

        'projects/<slug:{slug}>.json' => function($slug) {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'slug' => $slug,
                ],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $logo = !empty($entry->logo[0]) ? $entry->logo[0]->url : null;

                    $screensaver = [];
                    foreach ($entry->screensaver->all() as $block) {
                        switch ($block->type->handle) {
                            case 'singleImage':
                                $screensaver[] = [
                                    'asset' => $block->asset->one()->url,
                                    'heading' => $block->heading,
                                    'subheading' => $block->subheading,
                                ];
                                break;
                            case 'multipleImages':
                                $assets = [];
                                foreach ($block->assets->all() as $asset) {
                                    $assets[] = $asset->url;
                                }
                                $screensaver[] = [
                                    'assets' => $assets,
                                    'heading' => $block->heading,
                                    'subheading' => $block->subheading,
                                ];
                                break;
                            case 'video':
                                $screensaver[] = [
                                    'vimeoId' => $block->vimeoId,
                                    'heading' => $block->heading,
                                    'subheading' => $block->subheading,
                                ];
                                break;
                            case 'color':
                                $screensaver[] = [
                                    'color' => $block->color->getHex(),
                                    'heading' => $block->heading,
                                    'subheading' => $block->subheading,
                                ];
                                break;
                        }
                    }

                    $child = $entry->getChildren();
                    $slug = !empty($child[0]) ? $child[0]->slug : null;

                    return [
                        'entryType' => $entry->type->name,
                        'title' => $entry->title,
                        'url' => "/{$entry->slug}/{$slug}",
                        'logo' => $logo,
                        'headerColor' => $entry->headerColor->getHex(),
                        'theme' => $entry->darkLightTheme,
                        'screensaver' => $screensaver,
                        'jsonUrl' => UrlHelper::url("projects/main-menu/{$slug}.json"),
                    ];
                },
            ];
        },

        'projects/main-menu/<slug:{slug}>.json' => function($slug) {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'slug' => $slug,
                    'type' => 'mainMenu'
                ],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $project = $entry->ancestors->one();
                    $project = $project ? [
                        'title' => $project->title,
                        'logo' => !empty($project->logo[0]) ? $project->logo[0]->url : null,
                        'headerColor' => $project->headerColor->getHex()
                    ] : null;

                    $children = $entry->getChildren();
                    $entries = [];
                    foreach ($children->all() as $entry) {
                        $thumbnailBg = [];
                        if ($entry->thumbnailBg) {
                          foreach ($entry->thumbnailBg->all() as $block) {
                              switch ($block->type->handle) {
                                  case 'image':
                                      $thumbnailBg[] = [
                                          'type' => $block->type->handle,
                                          'asset' => $block->asset->one()->url,
                                      ];
                                      break;
                                  case 'video':
                                      $thumbnailBg[] = [
                                          'type' => $block->type->handle,
                                          'vimeoId' => $block->vimeoId,
                                      ];
                                      break;
                                  case 'color':
                                      $thumbnailBg[] = [
                                          'type' => $block->type->handle,
                                          'color' => $block->color->getHex(),
                                      ];
                                      break;
                              }
                          }
                        }

                        $ancestorsSlugs = null;
                        foreach ($entry->ancestors->all() as $ancestor) {
                            $ancestorsSlugs .= "/$ancestor->slug";
                        }

                        $slug = $entry->slug;
                        $type = $entry->type;
                        if ($entry->type->name == 'Folder') {
                          $jsonPath = "projects/main-menu/folder/$slug.json";
                        } else {
                          $jsonPath = "projects/media/$type/$slug.json";
                        }

                        $entries[] = [
                          'entryType' => $entry->type->name,
                          'title' => $entry->title,
                          'url' => parse_url($entry->url)['path'],
                          'thumbnailBg' => $thumbnailBg,
                          'jsonUrl' => UrlHelper::url($jsonPath),
                        ];
                    }

                    return [
                        'project' => $project,
                        'slides' => $entries,
                    ];
                },
            ];
        },

        'projects/folder/<slug:{slug}>.json' => function($slug) {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'slug' => $slug,
                    'type' => 'folder'
                ],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $project = $entry->ancestors->one();
                    $project = $project ? [
                        'headerColor' => $project->headerColor->getHex()
                    ] : null;

                    $parent = $entry->getParent();

                    // Breadcrumbs
                    $ancestors = [];
                    foreach ($entry->ancestors->all() as $ancestor) {
                        $ancestors[] = [
                          'title' => $ancestor->title,
                          'url' => parse_url($ancestor->url)['path'],
                        ];
                    }

                    // Current location
                    $ancestors[] = [
                      'title' => $entry->title,
                    ];

                    $children = $entry->getChildren();
                    $entries = [];
                    foreach ($children->all() as $media) {
                        $thumbnailBg = [];
                        if ($media->thumbnailBg) {
                          foreach ($media->thumbnailBg->all() as $block) {
                              switch ($block->type->handle) {
                                  case 'image':
                                      $thumbnailBg[] = [
                                          'asset' => $block->asset->one()->url,
                                      ];
                                      break;
                                  case 'video':
                                      $thumbnailBg[] = [
                                          'vimeoId' => $block->vimeoId,
                                      ];
                                      break;
                                  case 'color':
                                      $thumbnailBg[] = [
                                          'color' => $block->color->getHex(),
                                      ];
                                      break;
                              }
                          }
                        }

                        $entries[] = [
                          'title' => $media->title,
                          'url' => parse_url($media->url)['path'],
                          'type' => $media->type->name,
                          'thumbnailBg' => $thumbnailBg,
                        ];
                    }

                    return [
                        'entryType' => $entry->type->name,
                        'title' => $entry->title,
                        'project' => $project,
                        'backLink' => $parent->url,
                        'breadcrumbs' => $ancestors,
                        'slides' => $entries,
                    ];
                },
            ];
        },

        'projects/media/<slug:{slug}>.json' => function($slug) {
            return [
                'criteria' => [
                    'section' => 'projects',
                    'slug' => $slug,
                ],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    $project = $entry->ancestors->one();
                    $project = $project ? [
                        'headerColor' => $project->headerColor->getHex()
                    ] : null;

                    $parent = $entry->getParent();

                    $ancestors = [];
                    foreach ($entry->ancestors->all() as $ancestor) {
                        $ancestors[] = [
                          'title' => $ancestor->title,
                          'url' => $ancestor->url,
                        ];
                    }

                    $ancestors[] = [
                      'title' => $entry->title,
                    ];

                    switch ($entry->type) {
                        case 'video':
                            return [
                                'entryType' => $entry->type->name,
                                'project' => $project,
                                'backLink' => $parent->url,
                                'breadcrumbs' => $ancestors,
                                'vimeoId' => $entry->vimeoId,
                            ];
                            break;
                        case 'imageGallery':
                            $imageAssets = [];
                            foreach ($entry->imageAssets->all() as $asset) {
                                $imageAssets[] = $asset->url;
                            }

                            return [
                                'entryType' => $entry->type->name,
                                'project' => $project,
                                'backLink' => $parent->url,
                                'breadcrumbs' => $ancestors,
                                'imageAssets' => $imageAssets,
                            ];
                            break;
                        case 'pdf':
                            $pdf = !empty($entry->pdfAsset[0]) ? $entry->pdfAsset[0]->url : null;

                            return [
                                'entryType' => $entry->type->name,
                                'project' => $project,
                                'backLink' => $parent->url,
                                'breadcrumbs' => $ancestors,
                                'pdfAsset' => $pdf,
                            ];
                            break;
                        case 'website':
                            return [
                                'entryType' => $entry->type->name,
                                'project' => $project,
                                'backLink' => $parent->url,
                                'breadcrumbs' => $ancestors,
                                'siteUrl' => $entry->siteUrl,
                            ];
                            break;
                    }
                },
            ];
        },
    ]
];
