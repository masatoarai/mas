import { injectGlobal } from 'styled-components';

injectGlobal`
  html {
    -webkit-font-smoothing: antialiased;
  }


  html, body {
    height: 100%;
  }

  #root {
    height: 100%;
  }
`;
