import { css } from 'styled-components';

export const media = {
  tv: (...args) => css`
    @media (min-width: 1480px) {
      ${css(...args)};
    }
  `,
  desktop: (...args) => css`
    @media (min-width: 1180px) {
      ${css(...args)};
    }
  `,
};
