export default {
  projects: `${process.env.REACT_APP_API_PATH}/projects.json`,
  project: `${process.env.REACT_APP_API_PATH}/projects`,
};
