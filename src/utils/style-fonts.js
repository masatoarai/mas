import { injectGlobal } from 'styled-components';

import UniversLight from '../fonts/UniversLTStdLight.woff';
import UniversLTStd from '../fonts/UniversLTStd.woff';
import UrbanSplashLightRegular from '../fonts/UrbanSplashLightRegular.woff';
import UrbanSplashMediumRegular from '../fonts/UrbanSplashMediumRegular.woff';

injectGlobal`
  @font-face {
    font-family: UniversLight;
    src: url(${UniversLight}) format('woff');
  }

  @font-face {
    font-family: Univers;
    src: url(${UniversLTStd}) format('woff');
  }

  @font-face {
    font-family: UrbanSplashLight;
    src: url(${UrbanSplashLightRegular}) format('woff');
  }

  @font-face {
    font-family: UrbanSplashMedium;
    src: url(${UrbanSplashMediumRegular}) format('woff');
  }
`;
