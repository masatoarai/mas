// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

const root = document.getElementById('root');

if (root !== null) {
  ReactDOM.render(<App />, root);
}

registerServiceWorker();
