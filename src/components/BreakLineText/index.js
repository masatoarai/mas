// @flow

import React, { Component } from 'react';

type Props = {
  text: string,
};

class BreakLineText extends Component<Props> {
  render() {
    return (
      <React.Fragment>
        {this.props.text.split('\n').map((item, key) => {
          return (
            <span key={key}>
              {item}
              <br />
            </span>
          );
        })}
      </React.Fragment>
    );
  }
}

export default BreakLineText;
