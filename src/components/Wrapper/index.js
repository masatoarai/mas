// @flow

import styled from 'styled-components';
import { media } from '../../utils/style-media';

const Wrapper = styled.div`
  padding: 0 24px;

  ${media.tv`
    padding: 0 48px;
  `};
`;

export default Wrapper;
