// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import styled, { ThemeProvider } from 'styled-components';
import 'slick-carousel/slick/slick.css';
import { media } from '../../utils/style-media';
import Card, { CardContainer } from '../Card';
// import { PrevArrow } from '../CarouselUi';

import Display4, { theme } from '../Display4';
import Body from '../Body';
import KebabIcon from '../../icons/ic_kebab_white.svg';
import PrevIcon from '../../icons/ic_chevron_left_white.svg';
import NextIcon from '../../icons/ic_chevron_right_white.svg';
import RectangleShadow from '../../images/rectangle_shadow.png';

const CarouselHeader = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

const Ui = styled.div`
  display: flex;
`;

const NextPrev = styled.div`
  display: flex;
  margin-right: 24px;
`;

const KebabMenu = styled.div`
  position: relative;
`;

const UiItem = styled.div`
  align-items: center;
  background-color: #222729;
  cursor: pointer;
  display: flex;
  height: 48px;
  justify-content: center;
  user-select: none;
  width: 48px;

  &:first-child {
    margin-right: 1px;
  }

  ${media.tv`
    height: 64px;
    width: 64px;
  `};
`;

const Icon = styled.img`
  height: 24px;
  transition: all 0.1s;
  width: 24px;

  ${media.tv`
    height: 32px;
    width: 32px;
  `};
`;

const MenuBody = styled.div`
  bottom: calc(-1 * 48px);
  box-shadow: 0 2px 16px 1px rgba(34, 39, 41, 0.16);
  min-width: 260px;
  position: absolute;
  right: calc(-1 * 6px);
  transform: translateY(100%);
  transition: bottom 0.5s cubic-bezier(0.23, 1, 0.32, 1),
    opacity 0.2s cubic-bezier(0.165, 0.84, 0.44, 1),
    visibility 0.2s cubic-bezier(0.165, 0.84, 0.44, 1);
  width: 100%;
  z-index: 1;
`;

const Box = styled.div`
  background-color: #ffffff;
  padding: 24px;
  padding-bottom: 20px;
  position: relative;

  &::after {
    content: url(${RectangleShadow});
    position: absolute;
    right: 3px;
    top: -22px;
    z-index: -1;
  }
`;

const MenuList = styled.ul`
  list-style: none;
  margin: 0;
  padding-left: 0;
  user-select: none;
`;

const ListItem = styled.li`
  margin-bottom: 16px;
  opacity: 0.5;

  &:last-child {
    margin-bottom: 0;
  }
`;

const Slide = styled.div`
  vertical-align: middle;
`;

const CarouselSlider = styled(Slider)`
  margin-top: 37px;
  max-width: 50%;

  ${media.desktop`
    max-width: 33.3333%;
  `};

  ${media.tv`
    margin-top: 73px;
  `};

  & .slick-list {
    overflow: inherit;
  }

  & .slick-slide * {
    outline: none;
  }

  & ${CardContainer} {
    margin-right: 16px;
  }
`;

// const PrevArrow =({onClick}) => (
//   <UiItem onClick={onClick}>
//     <Icon src={PrevIcon} alt="Prev Arrow" />
//   </UiItem>
// )

type Props = {
  title: string,
  slides: [],
};

type State = {
  isHidden: boolean,
};

class Carousel extends Component<Props, State> {
  Props = {
    slides: [],
  };

  myRef: { current: null | HTMLDivElement } = React.createRef();

  state = {
    isHidden: true,
  };

  toggleHidden() {
    this.setState({
      isHidden: !this.state.isHidden,
    });
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false);
  }

  handleClick = (e: Event) => {
    // if (this.node.contains(e.target)) {
    //   return;
    // }
  };


  // slider: { current: null | HTMLDivElement } = React.createRef();

  next() {
    this.slickNext();
  }

  previous() {
    this.slider.slickPrev();
  }

  render() {
    const slides = this.props.slides;

    const settings = {
      cssEase: 'ease-in-out',
      dots: false,
      draggable: false,
      infinite: false,
      pauseOnHover: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 300,
      swipeToSlide: true,
    };

    return (
      <React.Fragment>
        <CarouselHeader>
          <ThemeProvider theme={theme}>
            <Display4>{this.props.title}</Display4>
          </ThemeProvider>
          <Ui>
            <NextPrev>
              <UiItem onClick={this.previous}>
                <Icon src={PrevIcon} alt="Prev Arrow" />
              </UiItem>
              <UiItem>
                <Icon src={NextIcon} alt="Next Arrow" />
              </UiItem>
            </NextPrev>
            <KebabMenu>
              <UiItem onClick={this.toggleHidden.bind(this)}>
                <Icon src={KebabIcon} alt="Kebab Icon" />
              </UiItem>
              {!this.state.isHidden && (
                <MenuBody ref={node => (this.myRef = node)}>
                  <Box>
                    <MenuList>
                      {this.props.slides.map((block, i) => (
                        <ListItem key={i}>
                          <Body>{block.title}</Body>
                        </ListItem>
                      ))}
                    </MenuList>
                  </Box>
                </MenuBody>
              )}
            </KebabMenu>
          </Ui>
        </CarouselHeader>

        <CarouselSlider ref={c => (this.slider = c)} {...settings}>
          {slides.map((block, i) => (
            <Slide key={i}>
              <Link to={`${block.url}`}>
                <Card
                  thumbnailBg={block.thumbnailBg}
                  entryType={block.entryType}
                  title={block.title}
                  thumbnailSubheading={block.thumbnailSubheading}
                />
              </Link>
            </Slide>
          ))}
        </CarouselSlider>
      </React.Fragment>
    );
  }
}

export default Carousel;
