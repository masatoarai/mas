// @flow

import React, { Component } from 'react';
import Display4, { theme } from '../Display4';
import { ThemeProvider } from 'styled-components';
import styled from 'styled-components';
import { media } from '../../utils/style-media';
import Body from '../Body';
import KebabIcon from '../../icons/ic_kebab_white.svg';
import PrevIcon from '../../icons/ic_chevron_left_white.svg';
import NextIcon from '../../icons/ic_chevron_right_white.svg';
import RectangleShadow from '../../images/rectangle_shadow.png';

const Container = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

const Ui = styled.div`
  display: flex;
`;

const NextPrev = styled.div`
  display: flex;
  margin-right: 24px;
`;

const KebabMenu = styled.div`
  position: relative;
`;

const UiItem = styled.div`
  align-items: center;
  background-color: #222729;
  cursor: pointer;
  display: flex;
  height: 48px;
  justify-content: center;
  user-select: none;
  width: 48px;

  &:first-child {
    margin-right: 1px;
  }

  ${media.tv`
    height: 64px;
    width: 64px;
  `};
`;

const Icon = styled.img`
  height: 24px;
  transition: all 0.1s;
  width: 24px;

  ${media.tv`
    height: 32px;
    width: 32px;
  `};
`;

const MenuBody = styled.div`
  bottom: calc(-1 * 48px);
  box-shadow: 0 2px 16px 1px rgba(34, 39, 41, 0.16);
  min-width: 260px;
  position: absolute;
  right: calc(-1 * 6px);
  transform: translateY(100%);
  transition: bottom 0.5s cubic-bezier(0.23, 1, 0.32, 1),
    opacity 0.2s cubic-bezier(0.165, 0.84, 0.44, 1),
    visibility 0.2s cubic-bezier(0.165, 0.84, 0.44, 1);
  width: 100%;
  z-index: 1;
`;

const Box = styled.div`
  background-color: #ffffff;
  padding: 24px;
  padding-bottom: 20px;
  position: relative;

  &::after {
    content: url(${RectangleShadow});
    position: absolute;
    right: 3px;
    top: -22px;
    z-index: -1;
  }
`;

const MenuList = styled.ul`
  list-style: none;
  margin: 0;
  padding-left: 0;
  user-select: none;
`;

const ListItem = styled.li`
  margin-bottom: 16px;
  opacity: 0.5;

  &:last-child {
    margin-bottom: 0;
  }
`;

type State = {
  isHidden: boolean,
};

type Props = {
  title: string,
  slides: [],
  // e: string,
};

class CarouselUi extends Component<Props, State> {
  myRef: { current: null | HTMLDivElement } = React.createRef();

  state = {
    isHidden: true,
  };

  toggleHidden() {
    this.setState({
      isHidden: !this.state.isHidden,
    });
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClick, false);
  }

  handleClick = (e: Event) => {
    // if (this.node.contains(e.target)) {
    //   return;
    // }
  };

  render() {
    return (
      <Container>
        <ThemeProvider theme={theme}>
          <Display4>{this.props.title}</Display4>
        </ThemeProvider>
        <Ui>
          <NextPrev>
            <UiItem className="carousel-ui-prev">
              <Icon src={PrevIcon} alt="Prev Arrow" />
            </UiItem>
            <UiItem>
              <Icon src={NextIcon} alt="Next Arrow" />
            </UiItem>
          </NextPrev>
          <KebabMenu>
            <UiItem onClick={this.toggleHidden.bind(this)}>
              <Icon src={KebabIcon} alt="Kebab Icon" />
            </UiItem>
            {!this.state.isHidden && (
              <MenuBody ref={node => (this.myRef = node)}>
                <Box>
                  <MenuList>
                    {this.props.slides.map((block, i) => (
                      <ListItem key={i}>
                        <Body>{block.title}</Body>
                      </ListItem>
                    ))}
                  </MenuList>
                </Box>
              </MenuBody>
            )}
          </KebabMenu>
        </Ui>
      </Container>
    );
  }
}

export default CarouselUi;
