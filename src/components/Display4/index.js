// @flow

import styled from 'styled-components';
import Display from '../Display';
import { media } from '../../utils/style-media';

export const theme = {
  fontSizeRegular: '56px',
  fontSizeLarge: '80px',
  lineHeightRegular: '61px',
  lineHeightLarge: '85px',
};

const Display4 = styled(Display)`
  font-size: ${props => props.theme.fontSizeRegular};
  line-height: ${props => props.theme.lineHeightRegular};

  ${media.tv`
    font-size: ${props => props.theme.fontSizeLarge};
    line-height: ${props => props.theme.lineHeightLarge};
  `};
`;

export default Display4;
