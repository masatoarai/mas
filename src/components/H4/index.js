// @flow

import styled from 'styled-components';

const H4 = styled.h4`
  margin: 0;
`;

export default H4;
