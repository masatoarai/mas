// @flow

import styled from 'styled-components';

const Body = styled.span`
  font-family: UniversLight;
  font-size: 14px;
  letter-spacing: 0;
  line-height: 19px;
`;

export default Body;
