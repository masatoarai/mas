// @flow

import React, { Component } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import H3 from '../H3';
import H4 from '../H4';
import ArrowForward from '../../icons/ic_arrow_forward_white_small.svg';
import BgVideo from '../BgVideo';

export const CardContainer = styled.div`
  color: #ffffff;
  position: relative;
  padding-bottom: 75%;
  height: 0;
`;

const Scrim = styled.div`
  background: linear-gradient(135deg, #222729, rgba(34, 39, 41, 0));
  height: 100%;
  left: 0;
  opacity: 0.7;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
`;

const Background = styled.div`
  height: 100%;
  left: 0;
  top: 0;
  overflow: hidden;
  position: absolute;
  width: 100%;
  z-index: 0;
`;

const BackgroundImage = styled.div`
  background-image: url(${props => props.bgAsset});
  background-position: 50%;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100%;
  width: 100%;
`;

const BackgroundVideo = styled.div`
  color: black;
  height: 100%;
`;

const BackgroundColor = styled.div`
  background-color: ${props => props.bgColor};
  height: 100%;
  width: 100%;
`;

const InnerContent = styled.div`
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 2;
`;

const InnerContentWrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-between;
  padding: 22px 24px;
`;

const CategoryTag = styled.div`
  font-family: Univers;
  font-size: 12px;
  margin-bottom: 13px;
`;

const Heading = styled(H3)`
  font-family: UrbanSplashLight;
  font-size: 45px;
  font-weight: 400;
  line-height: 50px;
`;

const Subheading = styled(H4)`
  font-family: UrbanSplashLight;
  font-size: 45px;
  font-weight: 400;
  line-height: 50px;
  opacity: 0.7;
`;

const CardBottomContent = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

const CardBottomLabel = styled.span`
  font-family: UrbanSplashMedium;
  letter-spacing: 0.08em;
  text-transform: uppercase;
`;

const CardBottomIcon = styled.img`
  vertical-align: middle;
  width: 24px;
`;

const ratio4x3 = {
  main: "78%"
};

type Props = {
  thumbnailBg: [],
  entryType: string,
  title: string,
  thumbnailSubheading: string,
};

class Card extends Component<Props> {
  render() {
    return (
      <CardContainer>
        <Scrim />
        <Background>
          {this.props.thumbnailBg.map((bg, i) => {
            switch (bg.type) {
              case 'image':
                return <BackgroundImage key={i} bgAsset={bg.asset} />;
              case 'video':
                return (
                  <BackgroundVideo key={i}>
                    <ThemeProvider theme={ratio4x3}>
                      <BgVideo vimeoId={bg.vimeoId} />
                    </ThemeProvider>
                  </BackgroundVideo>
                );
              case 'color':
                return <BackgroundColor key={i} bgColor={bg.color} />;
              default:
            }
            return false;
          })}
        </Background>
        <InnerContent>
          <InnerContentWrapper>
            <div>
              <CategoryTag>{this.props.entryType}</CategoryTag>
              <Heading>{this.props.title}</Heading>
              <Subheading>{this.props.thumbnailSubheading}</Subheading>
            </div>
            <CardBottomContent>
              <CardBottomLabel>Explore</CardBottomLabel>
              <CardBottomIcon src={ArrowForward} alt="Arrow Forward" />
            </CardBottomContent>
          </InnerContentWrapper>
        </InnerContent>
      </CardContainer>
    );
  }
}

export default Card;
