// @flow

import styled from 'styled-components';
import H2 from '../H2';

const Display = styled(H2)`
  font-family: UrbanSplashLight;
  font-weight: 400;
`;

export default Display;
