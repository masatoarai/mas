// @flow

import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'normalize.css';
import '../../styles/global';
import Projects from '../../containers/Projects';
import Project from '../../containers/Project';
import MainMenu from '../../containers/MainMenu';
import FolderOrMedia from '../../containers/FolderOrMedia';
import Media from '../../containers/Media';

class App extends Component<{}> {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Projects} />
          <Route exact path="/:project" component={Project} />
          <Route exact path="/:project/:mainMenu" component={MainMenu} />
          <Route
            exact
            path="/:project/:mainMenu/:folderOrMedia"
            component={FolderOrMedia}
          />
          <Route
            exact
            path="/:project/:mainMenu/:folder/:media"
            component={Media}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
