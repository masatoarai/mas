// @flow

import styled from 'styled-components';
import Display from '../Display';
import { media } from '../../utils/style-media';

export const theme = {
  fontSizeRegular: '112px',
  fontSizeLarge: '144px',
  lineHeightRegular: '117px',
  lineHeightLarge: '144px;',
};

const Display5 = styled(Display)`
  font-size: ${props => props.theme.fontSizeRegular};
  line-height: ${props => props.theme.lineHeightRegular};

  ${media.tv`
    font-size: ${props => props.theme.fontSizeLarge};
    line-height: ${props => props.theme.lineHeightLarge};
  `};
`;

export default Display5;
