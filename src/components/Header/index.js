// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { media } from '../../utils/style-media';

const Container = styled.header`
  align-items: center;
  background-color: ${props => props.bgColor};
  display: flex;
  height: 64px;
  position: relative;
  z-index: 10;

  ${media.tv`
    height: 88px;
  `};
`;

const Wrapper = styled.div`
  padding: 0 24px;
  width: 100%;
  ${media.tv`
    padding: 0 48px;
  `};
`;

const Logo = styled.img`
  display: block;
  max-height: 32px;
  max-width: 200px;
`;

const StyledLink = styled(Link)`
  display: inline-block;
  text-decoration: none;
  vertical-align: middle;
`;

const Title = styled.h1`
  color: ${props => (props.theme === 'dark' ? '#ffffff' : '#000000')};
  font-family: UrbanSplashLight, Arial, sans-serif;
  font-size: 32px;
  font-weight: 400;
  line-height: 32px;
`;

type Props = {
  projectTitle: string,
  projectLogo: string,
  headerColor: string,
  projectTheme: string,
};

function ProjectTitle(props) {
  const logo = props.logo;
  if (logo) {
    return <Logo src={props.logo} alt={props.title} />;
  }
  return <Title theme={props.theme}>{props.title}</Title>;
}

class Header extends Component<Props> {
  render() {
    return (
      <Container bgColor={this.props.headerColor}>
        <Wrapper>
          <StyledLink to="/">
            <ProjectTitle
              logo={this.props.projectLogo}
              title={this.props.projectTitle}
              theme={this.props.projectTheme}
            />
          </StyledLink>
        </Wrapper>
      </Container>
    );
  }
}

export default Header;
