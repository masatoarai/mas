// @flow

import styled from 'styled-components';
import { media } from '../../utils/style-media';

const Content = styled.div`
  height: calc(100% - 136px);
  overflow-x: hidden;
  padding-bottom: 24px;
  padding-top: 48px;

  ${media.tv`
    padding-bottom: 48px;
    padding-top: 82px;
  `};
`;

export default Content;
