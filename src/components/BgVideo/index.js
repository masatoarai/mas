// @flow

import React, { Component } from 'react';
import styled from 'styled-components';

const CoverVideo = styled.div`
  position: relative;
  padding-bottom: ${props => props.theme.main};
  height: 0;

  & > iframe {
    border: 0;
    position: absolute;
    top: 50%;
    height: auto;
    left: 50%;
    max-width: inherit;
    min-width: 100%;
    min-height: 100%;
    transform: translate(-50%, -50%);
  	width: 100%;
  	height: 100%;
  }
`;

CoverVideo.defaultProps = {
  theme: {
    main: '56.25%'
  }
}

type Props = {
  vimeoId: string,
};

class BgVideo extends Component<Props> {
  setWidth() {
    const elements = document.querySelectorAll('.bg-cover-item');
    Array.prototype.forEach.call(elements, function(el, i) {
      const parentWidth = el.closest('.bg-cover').offsetWidth;
      const foo = parentWidth * 2.2;
      el.style.width = `${foo}px`;
    });
  }

  componentDidMount() {
    this.setWidth();
    window.addEventListener('resize', this.setWidth.bind(this));
  }

  render() {
    const vimeoUrl = `https://player.vimeo.com/video/${
      this.props.vimeoId
    }?background=1`;

    return (
      <CoverVideo className="bg-cover">
        <iframe
          src={vimeoUrl}
          className="bg-cover-item"
          title={vimeoUrl}
          frameBorder="0"
          webkitallowfullscreen="true"
          mozallowfullscreen="true"
          allowFullScreen
        />
      </CoverVideo>
    );
  }
}

export default BgVideo;
