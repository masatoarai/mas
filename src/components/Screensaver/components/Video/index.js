// @flow

import React, { Component } from 'react';
import Background from '../Background';
import BgVideo from '../../../BgVideo';
import Scrim from '../Scrim';
import InnerContent from '../InnerContent';

type Props = {
  vimeoId: string,
  heading: string,
  subheading: string,
};

class Video extends Component<Props> {
  render() {
    return (
      <React.Fragment>
        <Background>
          <BgVideo vimeoId={this.props.vimeoId} />
        </Background>
        <Scrim />
        <InnerContent
          heading={this.props.heading}
          subheading={this.props.subheading}
        />
      </React.Fragment>
    );
  }
}

export default Video;
