// @flow

import React, { Component } from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';
import Background from '../Background';
import BgImage from '../BgImage';
import Scrim from '../Scrim';
import InnerContent from '../InnerContent';
import 'slick-carousel/slick/slick.css';

type Props = {
  assets: [],
  heading: string,
  subheading: string,
};

const SlickSlider = styled(Slider)`
  &,
  & > div,
  & > div > div,
  & > div > div > div,
  & > div > div > div > div,
  & > div > div > div > div > div {
    height: 100%;
  }
`;

class MultipleImages extends Component<Props> {
  render() {
    const settings = {
      arrows: false,
      autoplay: true,
      autoplaySpeed: 3000,
      cssEase: 'ease-in-out',
      dots: false,
      fade: true,
      infinite: true,
      pauseOnHover: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
    };

    return (
      <div>
        <Background>
          <SlickSlider {...settings}>
            {this.props.assets.map((block, i) => (
              <BgImage key={i} bgAsset={block} />
            ))}
          </SlickSlider>
        </Background>
        <Scrim />
        <InnerContent
          heading={this.props.heading}
          subheading={this.props.subheading}
        />
      </div>
    );
  }
}

export default MultipleImages;
