// @flow

import React, { Component } from 'react';
import styled from 'styled-components';
import Background from '../Background';
import Scrim from '../Scrim';
import InnerContent from '../InnerContent';

type Props = {
  bgColor: string,
  heading: string,
  subheading: string,
};

const BgColor = styled.div`
  background-color: ${props => props.bgColor};
  height: 100%;
`;

class Color extends Component<Props> {
  render() {
    return (
      <div>
        <Background>
          <BgColor bgColor={this.props.bgColor} />
        </Background>
        <Scrim />
        <InnerContent
          heading={this.props.heading}
          subheading={this.props.subheading}
        />
      </div>
    );
  }
}

export default Color;
