// @flow

import React, { Component } from 'react';
import Background from '../Background';
import BgImage from '../BgImage';
import Scrim from '../Scrim';
import InnerContent from '../InnerContent';

type Props = {
  asset: string,
  heading: string,
  subheading: string,
};

class SingleImage extends Component<Props> {
  render() {
    return (
      <div>
        <Background>
          <BgImage bgAsset={this.props.asset} />
        </Background>
        <Scrim />
        <InnerContent
          heading={this.props.heading}
          subheading={this.props.subheading}
        />
      </div>
    );
  }
}

export default SingleImage;
