// @flow

import styled from 'styled-components';

const Scrim = styled.div`
  background: linear-gradient(135deg, #222729, rgba(34, 39, 41, 0));
  bottom: 0;
  left: 0;
  opacity: 0.7;
  position: absolute;
  right: 0;
  top: 0;
  width: 100%;
  z-index: 1;
`;

export default Scrim;
