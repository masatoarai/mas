// @flow

import styled from 'styled-components';

const BgImage = styled.div`
  background-image: url(${props => props.bgAsset});
  background-repeat: no-repeat;
  background-position: 50%;
  background-size: cover;
  height: 100%;
`;

export default BgImage;
