// @flow

import React, { Component } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { media } from '../../../../utils/style-media';
import Wrapper from '../../../Wrapper';
import Display5, { theme } from '../../../Display5';
import ArrowIcon from '../../../../icons/ic_arrow_forward_white.svg';
import BreakLineText from '../../../BreakLineText';

const Container = styled.div`
  bottom: 0;
  height: 100%;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  z-index: 2;
`;

const InnerContentWrapper = styled(Wrapper)`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-between;
`;

const InnerContentBody = styled.div`
  margin-top: 29px;
  max-width: 66%;

  ${media.tv`
    margin-top: 75px;
    max-width: 50%;
  `};
`;

const Heading = styled(Display5)`
  color: #ffffff;
`;

const Subheading = styled(Display5)`
  color: #ffffff;
  opacity: 0.7;
`;

const Arrow = styled.div`
  align-items: center;
  background-color: #222729;
  bottom: 24px;
  display: flex;
  height: 96px;
  justify-content: center;
  position: absolute;
  right: 24px;
  width: 96px;

  ${media.tv`
    bottom: 48px;
    height: 144px;
    right: 48px;
    width: 144px;
  `};
`;

const ArrowImg = styled.img`
  height: 48px;
  width: 48px;

  ${media.tv`
    height: 96px;
    width: 96px;
  `};
`;

type Props = {
  heading: string,
  subheading: string,
};

class InnerContent extends Component<Props> {
  render() {
    return (
      <Container>
        <InnerContentWrapper>
          <InnerContentBody>
            <ThemeProvider theme={theme}>
              <Heading>
                <BreakLineText text={this.props.heading} />
              </Heading>
            </ThemeProvider>
            <ThemeProvider theme={theme}>
              <Subheading>
                <BreakLineText text={this.props.subheading} />
              </Subheading>
            </ThemeProvider>
          </InnerContentBody>
          <Arrow>
            <ArrowImg src={ArrowIcon} alt="Arrow Foward Icon" />
          </Arrow>
        </InnerContentWrapper>
      </Container>
    );
  }
}

export default InnerContent;
