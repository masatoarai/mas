// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { media } from '../../utils/style-media';
import SingleImage from './components/SingleImage';
import MultipleImages from './components/MultipleImages';
import Video from './components/Video';
import Color from './components/Color';

const Container = styled.div`
  height: calc(100% - 64px);
  overflow-x: hidden;

  ${media.tv`
    height: calc(100% - 88px);
  `};
`;

const ScreensaverWrapper = styled.div`
  height: 100%;
  position: relative;
  overflow: hidden;
`;

const StyledLink = styled(Link)`
  display: block;
  height: 100%;
`;

function Slide(props) {
  switch (props.type) {
    case 'singleImage':
      return (
        <SingleImage
          asset={props.asset}
          heading={props.heading}
          subheading={props.subheading}
        />
      );
    case 'multipleImages':
      return (
        <MultipleImages
          assets={props.assets}
          heading={props.heading}
          subheading={props.subheading}
        />
      );
    case 'video':
      return (
        <Video
          vimeoId={props.vimeoId}
          heading={props.heading}
          subheading={props.subheading}
        />
      );
    case 'color':
      return (
        <Color
          bgColor={props.color}
          heading={props.heading}
          subheading={props.subheading}
        />
      );
    default:
  }
}

type Props = {
  projectUrl: string,
  screensaver: [],
};

class Screensaver extends Component<Props> {
  render() {
    const screensaver = this.props.screensaver;
    const Screensaver = screensaver.map((block, i) => (
      <Slide
        key={i}
        type={block.type}
        asset={block.asset}
        assets={block.assets}
        color={block.color}
        vimeoId={block.vimeoId}
        heading={block.heading}
        subheading={block.subheading}
      />
    ));

    return (
      <Container>
        <ScreensaverWrapper>
          <StyledLink to={`${this.props.projectUrl}`}>{Screensaver}</StyledLink>
        </ScreensaverWrapper>
      </Container>
    );
  }
}

export default Screensaver;
