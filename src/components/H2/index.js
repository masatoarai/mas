// @flow

import styled from 'styled-components';

const H2 = styled.h2`
  margin: 0;
`;

export default H2;
