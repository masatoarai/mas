// @flow

import React, { Component } from 'react';
import axios from 'axios';
import api from '../utils/api';
import Header from '../components/Header';
import Screensaver from '../components/Screensaver';

type State = {
  project: {
    entryType: string,
    title: string,
    url: string,
    logo: string,
    headerColor: string,
    theme: boolean,
  },
  screensaver: [],
};

const applyState = (project: {}, screensaver: []): Function => () => ({
  project,
  screensaver,
});

class Project extends Component<void, State> {
  state = {
    project: {
      entryType: '',
      title: '',
      url: '',
      logo: '',
      headerColor: '',
      theme: false,
    },
    screensaver: [],
  };

  componentDidMount() {
    const pathArray = window.location.pathname.split('/');
    const lastSegment = pathArray[pathArray.length - 1];

    axios.get(`${api.project}/${lastSegment}.json`).then(res => {
      const project = res.data;
      const screensaver = res.data.screensaver;

      this.setState(applyState(project, screensaver));
    });
  }

  render() {
    const project = this.state.project;
    const screensaver = this.state.screensaver;

    return (
      <React.Fragment>
        <Header
          projectTitle={project.title}
          projectLogo={project.logo}
          headerColor={project.headerColor}
          projectTheme={project.theme === true ? 'dark' : 'light'}
        />
        <Screensaver
          projectUrl={project.url}
          screensaver={screensaver}
        />
      </React.Fragment>
    );
  }
}

export default Project;
