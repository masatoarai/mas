// @flow

import React, { Component } from 'react';
import axios from 'axios';
// import { Link } from 'react-router-dom';
import api from '../utils/api';
import styled from 'styled-components';
import Header from '../components/Header';
import Content from '../components/Content';
import Wrapper from '../components/Wrapper';
// import CarouselUi from '../components/CarouselUi';
import Carousel from '../components/Carousel';

const ContentWrapper = styled(Wrapper)`
  height: 100%;
  position: relative;
`;

type State = {
  project: {
    title: string,
    projectTitle: string,
    logo: string,
    headerColor: string,
    theme: boolean,
  },
  slides: [],
};

const applyState = (project: {}, slides: []): Function => () => ({
  project,
  slides,
});

class MainMenu extends Component<void, State> {
  state = {
    project: {
      title: '',
      projectTitle: '',
      logo: '',
      headerColor: '',
      theme: false,
    },
    slides: [],
  };

  componentDidMount() {
    const pathArray = window.location.pathname.split('/');
    const lastSegment = pathArray[pathArray.length - 1];

    axios.get(`${api.project}/${lastSegment}.json`).then(res => {
      const project = res.data;
      const slides = res.data.slides;

      this.setState(applyState(project, slides));
    });
  }

  render() {
    const project = this.state.project;
    const slides = this.state.slides;

    return (
      <React.Fragment>
        <Header
          projectTitle={project.title}
          projectLogo={project.logo}
          headerColor={project.headerColor}
          projectTheme={project.theme === true ? 'dark' : 'light'}
        />

        <Content>
          <ContentWrapper>
            {/*<CarouselUi title={project.title} slides={slides} />*/}
            <Carousel title={project.title} slides={slides} />
          </ContentWrapper>
        </Content>
      </React.Fragment>
    );
  }
}

export default MainMenu;
