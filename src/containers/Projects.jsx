// @flow

import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import api from '../utils/api';
import styled from 'styled-components';

import '../utils/style-fonts';
import { media } from '../utils/style-media';

const List = styled.ul`
  margin: 0;
  padding: 24px;

  ${media.tv`
    padding: 48px;
  `};
`;

const ProjectItem = styled.li`
  display: inline-block;
  font-size: 112px;
  line-height: 117px;
  font-family: UrbanSplashLight, Arial, sans-serif;

  ${media.tv`
    font-size: 144px;
    line-height: 144px;
  `};
`;

const Slash = styled.span`
  margin: 0 18px;
`;

const StyledLink = styled(Link)`
  color: inherit;
  text-decoration: none;
`;

type State = {
  projects: [],
};

const applyProjects = (projects: []): Function => () => ({
  projects,
});

class Project extends Component<void, State> {
  state = { projects: [] };

  componentDidMount() {
    axios.get(api.projects).then(res => {
      const projects = res.data.data;
      this.setState(applyProjects(projects));
    });
  }

  render() {
    const projects = this.state.projects;

    const listItems = projects.map((project, i) => (
      <ProjectItem key={project.slug}>
        {i ? <Slash>/</Slash> : ''}
        <StyledLink to={`/${project.slug}`}>{project.title}</StyledLink>
      </ProjectItem>
    ));

    return (
      <React.Fragment>
        <List>{listItems}</List>
      </React.Fragment>
    );
  }
}

export default Project;
