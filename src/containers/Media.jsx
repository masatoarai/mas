// @flow

import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import api from '../utils/api';

type State = {
  project: {
    entryType: string,
    title: string,
    headerColor: string,
    backLink: string,
    siteUrl: string,
    pdfAsset: string,
    issuuUrl: string,
    vimeoId: string,
    imageAssets: [],
  },
  breadcrumbs: [],
};

const applyState = (project: {}, breadcrumbs: []): Function => () => ({
  project,
  breadcrumbs,
});

class Media extends Component<void, State> {
  state = {
    project: {
      entryType: '',
      title: '',
      headerColor: '',
      backLink: '',
      siteUrl: '',
      pdfAsset: '',
      issuuUrl: '',
      vimeoId: '',
      imageAssets: [],
    },
    breadcrumbs: [],
  };

  componentDidMount() {
    const pathArray = window.location.pathname.split('/');
    const lastSegment = pathArray[pathArray.length - 1];

    axios.get(`${api.project}/${lastSegment}.json`).then(res => {
      const project = res.data;
      const breadcrumbs = res.data.breadcrumbs;

      this.setState(applyState(project, breadcrumbs));
    });
  }

  render() {
    const project = this.state.project;
    const breadcrumbs = this.state.breadcrumbs;

    const items = breadcrumbs.map((breadcrumb, i) => {
      let item;
      if (i !== breadcrumbs.length - 1) {
        item = <Link to={`${breadcrumb.url}`}>{breadcrumb.title}</Link>;
      } else {
        item = <span>{breadcrumb.title}</span>;
      }
      return <li key={i}>{item}</li>;
    });

    const Breadcrumbs = <ul>{items}</ul>;

    let Media;
    if (project.entryType === 'Website') {
      Media = <div>Site URL: {project.siteUrl}</div>;
    } else if (project.entryType === 'PDF') {
      Media = (
        <ul>
          <li>PDF Asset: {project.pdfAsset}</li>
          <li>ISSUU URL: {project.issuuUrl}</li>
        </ul>
      );
    } else if (project.entryType === 'Video') {
      Media = <div>Vimeo ID: {project.vimeoId}</div>;
    } else if (project.entryType === 'Image Gallery') {
      Media = (
        <div>
          Image Assets:
          <ul>
            {project.imageAssets.map((asset, i) => {
              return <li key={i}>{asset}</li>;
            })}
          </ul>
        </div>
      );
    }

    return (
      <div>
        <ul>
          <li>Entry Type: {project.entryType}</li>
          <li>Header Color: {project.headerColor}</li>
          <li>Back Link: {project.backLink}</li>
          <li>Breadcrumbs: {Breadcrumbs}</li>
        </ul>

        {Media}
      </div>
    );
  }
}

export default Media;
