// @flow

import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import api from '../utils/api';

type State = {
  project: {
    entryType: string,
    title: string,
    headerColor: string,
    backLink: string,
    siteUrl: string,
    pdfAsset: string,
    issuuUrl: string,
    vimeoId: string,
    imageAssets: [],
  },
  breadcrumbs: [],
  slides: [
    {
      entryType: string,
      title: string,
      thumbnailSubheading: string,
      url: string,
      thumbnailBg: [
        {
          type: string,
          asset: string,
          vimeoId: string,
          color: string,
        },
      ],
    },
  ],
};

const applyState = (
  project: {},
  breadcrumbs: [],
  slides: []
): Function => () => ({
  project,
  breadcrumbs,
  slides,
});

class FolderOrMedia extends Component<void, State> {
  state = {
    project: {
      entryType: '',
      title: '',
      headerColor: '',
      backLink: '',
      siteUrl: '',
      pdfAsset: '',
      issuuUrl: '',
      vimeoId: '',
      imageAssets: [],
    },
    breadcrumbs: [],
    slides: [
      {
        entryType: '',
        title: '',
        thumbnailSubheading: '',
        url: '',
        thumbnailBg: [
          {
            type: '',
            asset: '',
            vimeoId: '',
            color: '',
          },
        ],
      },
    ],
  };

  componentDidMount() {
    const pathArray = window.location.pathname.split('/');
    const lastSegment = pathArray[pathArray.length - 1];

    axios.get(`${api.project}/${lastSegment}.json`).then(res => {
      const project = res.data;
      const breadcrumbs = res.data.breadcrumbs;
      const slides = res.data.slides;

      this.setState(applyState(project, breadcrumbs, slides));
    });
  }

  bgImage(bg: { asset: string }, param: string) {
    return <li>{bg.asset}</li>;
  }

  bgVideo(bg: { vimeoId: string }, param: string) {
    return <li>{bg.vimeoId}</li>;
  }

  bgColor(bg: { color: string }, param: string) {
    return <li>{bg.color}</li>;
  }

  render() {
    const project = this.state.project;
    const breadcrumbs = this.state.breadcrumbs;
    const slides = this.state.slides;

    const items = breadcrumbs.map((breadcrumb, i) => {
      let item;
      if (i !== breadcrumbs.length - 1) {
        item = <Link to={`${breadcrumb.url}`}>{breadcrumb.title}</Link>;
      } else {
        item = <span>{breadcrumb.title}</span>;
      }
      return <li key={i}>{item}</li>;
    });

    const Breadcrumbs = <ul>{items}</ul>;

    let Slides;
    let Media;
    if (project.entryType === 'Folder') {
      Slides = slides.map((block, i) => (
        <ul key={i}>
          <li>{block.entryType}</li>
          <li>{block.title}</li>
          <li>
            <Link to={`${block.url}`}>{block.url}</Link>
          </li>
          <li>
            {block.thumbnailBg.map((bg, i) => {
              switch (bg.type) {
                case 'image':
                  return (
                    <ul key={i}>
                      <li>{bg.type}</li>
                      {this.bgImage(bg, bg.type)}
                    </ul>
                  );
                case 'video':
                  return (
                    <ul key={i}>
                      <li>{bg.type}</li>
                      {this.bgVideo(bg, bg.type)}
                    </ul>
                  );
                case 'color':
                  return (
                    <ul key={i}>
                      <li>{bg.type}</li>
                      {this.bgColor(bg, bg.type)}
                    </ul>
                  );
                default:
              }
              return false;
            })}
          </li>
        </ul>
      ));
    } else if (project.entryType === 'Website') {
      Media = <div>Site URL: {project.siteUrl}</div>;
    } else if (project.entryType === 'PDF') {
      Media = (
        <ul>
          <li>PDF Asset: {project.pdfAsset}</li>
          <li>ISSUU URL: {project.issuuUrl}</li>
        </ul>
      );
    } else if (project.entryType === 'Video') {
      Media = <div>Vimeo ID: {project.vimeoId}</div>;
    } else if (project.entryType === 'Image Gallery') {
      Media = (
        <div>
          Image Assets:
          <ul>
            {project.imageAssets.map((asset, i) => {
              return <li key={i}>{asset}</li>;
            })}
          </ul>
        </div>
      );
    }

    return (
      <div>
        <ul>
          <li>Entry Type: {project.entryType}</li>
          <li>Title: {project.title}</li>
          <li>Header Color: {project.headerColor}</li>
          <li>Back Link: {project.backLink}</li>
          <li>Breadcrumbs: {Breadcrumbs}</li>
        </ul>

        {Slides}
        {Media}
      </div>
    );
  }
}

export default FolderOrMedia;
